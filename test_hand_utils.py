from hand_utils import has_x_same_value, is_following_value, is_same_color, has_x_y_same_value, calcul_hand, compare_hand

hand_royal_flush = {
    "color": ['C','C','C','C','C'],
    "value": ['A', 'J', 'Q', 'K', 'T']
}

hand_straight_flush = {
    "color": ['C','C','C','C','C'],
    "value": ['5', '7', '9', '6', '8']
}

hand_quads = {
    "color": ['C','D','H','C','S'],
    "value": ['A', 'A', 'A', '5', 'A']
}

hand_full = {
    "color": ['C', 'D', 'C', 'H', 'S'],
    "value": ['3', '3', '5', '5', '3']
}

hand_flush = {
    "color": ['C','C','C','C','C'],
    "value": ['3', '9', '7', 'T', 'A']
}

hand_straight = {
    "color": ['C', 'D', 'C', 'H', 'S'],
    "value": ['5','6','7','8','9']
}

hand_set = {
    "color": ['C','D','H','C','S'],
    "value": ['J', 'K', 'J', '5', 'J']
}

hand_twopair = {
    "color": ['C','D','H','D','C'],
    "value": ['T', 'T', '6', '6', 'K']
}

hand_onepair = {
    "color": ['C','D','H','S','C'],
    "value": ['A', '8', 'A', '2', '6']
}

hand_highcard = {
    "color": ['C','D','H','S','C'],
    "value": ['K', 'J', 'T', '6', '3']
}


def test_has_x_same_value():
    assert has_x_same_value(hand_onepair) == ('A', 2)
    assert has_x_same_value(hand_set) == ('J', 3)


def test_is_following_value():
    assert is_following_value(hand_straight)
    assert not is_following_value(hand_full)


def test_is_same_color():
    assert is_same_color(hand_flush)
    assert not is_same_color(hand_straight)


def test_has_x_y_same_value():
    assert has_x_y_same_value(hand_full) == [('3', 3), ('5', 2)]
    assert has_x_y_same_value(hand_twopair) == [('T', 2), ('6', 2)]
    assert has_x_y_same_value(hand_onepair) == [('A', 2)]


def test_calcul_hand():
    assert calcul_hand(hand_royal_flush) == ('Quinte Flush', 'A')
    assert calcul_hand(hand_straight_flush) == ('Quinte Flush', '9')
    assert calcul_hand(hand_quads) == ('Carré', 'A', '5')
    assert calcul_hand(hand_full) == ('Full', '3', '5')
    assert calcul_hand(hand_flush) == ('Couleur','A', 'T', '9', '7', '3')
    assert calcul_hand(hand_straight) == ('Quinte', '9')
    assert calcul_hand(hand_set) == ('Brelan', 'J', 'K', '5')
    assert calcul_hand(hand_twopair) == ('Double Paire', 'T', '6', 'K')
    assert calcul_hand(hand_onepair) == ('Paire', 'A', '8', '6', '2')
    assert calcul_hand(hand_highcard) == ('Hauteur', 'K', 'J', 'T', '6', '3')


# Different hand for make difficulties to compare
hand_quads_ace = {
    "color": ['C','D','S','H','S'],
    "value": ['A', 'A', '9', 'A', 'A']
}
hand_quads_nine = {
    "color": ['C','D','H','C','S'],
    "value": ['9', '9', '9', '7', '9']
}
hand_full_five = {
    "color": ['C', 'D', 'C', 'H', 'S'],
    "value": ['3', '5', '5', '5', '3']
}
hand_full_three_seven = {
    "color": ['C', 'D', 'C', 'H', 'S'],
    "value": ['3', '3', '7', '7', '3']
}
hand_flush_queen = {
    "color": ['C','C','C','C','C'],
    "value": ['3', '9', 'Q', 'T', '7']
}
hand_straight_queen = {
    "color": ['C', 'D', 'C', 'H', 'S'],
    "value": ['Q','T','J','8','9']
}
hand_set_jack_three = {
    "color": ['C','D','H','C','S'],
    "value": ['J', 'K', 'J', '3', 'J']
}
hand_set_ace = {
    "color": ['C','D','H','C','S'],
    "value": ['A', 'K', 'A', '5', 'A']
}
hand_twopair_nine_eight = {
    "color": ['C','D','H','D','C'],
    "value": ['9', '8', '8', '9', 'K']
}
hand_twopair_queen = {
    "color": ['C','D','H','D','C'],
    "value": ['T', 'T', '6', '6', 'Q']
}
hand_onepair_ace_seven = {
    "color": ['C','D','H','S','C'],
    "value": ['A', '8', 'A', '2', '7']
}
hand_onepair_queen = {
    "color": ['C','D','H','S','C'],
    "value": ['Q', 'Q', 'J', '2', '6']
}
hand_highcard_queen = {
    "color": ['C','D','H','S','C'],
    "value": ['9', 'J', 'Q', 'K', '3']
}


def test_compare_hand():
    assert compare_hand(hand_onepair, hand_twopair)[0] == 2
    assert compare_hand(hand_flush, hand_highcard)[0] == 1
    assert compare_hand(hand_royal_flush, hand_straight_flush)[0] == 1
    assert compare_hand(hand_quads, hand_quads_ace)[0] == 2
    assert compare_hand(hand_quads, hand_quads_nine)[0] == 1
    assert compare_hand(hand_full, hand_full_five)[0] == 2
    assert compare_hand(hand_full, hand_full_three_seven)[0] == 2
    assert compare_hand(hand_flush, hand_flush_queen)[0] == 1
    assert compare_hand(hand_straight, hand_straight_queen)[0] == 2
    assert compare_hand(hand_set, hand_set_jack_three)[0] == 1
    assert compare_hand(hand_set, hand_set_ace)[0] == 2
    assert compare_hand(hand_twopair, hand_twopair_nine_eight)[0] == 1
    assert compare_hand(hand_twopair, hand_twopair_queen)[0] == 1
    assert compare_hand(hand_onepair, hand_onepair_queen)[0] == 1
    assert compare_hand(hand_onepair, hand_onepair_ace_seven)[0] == 2
    assert compare_hand(hand_highcard, hand_highcard_queen)[0] == 2
    assert compare_hand(hand_flush, hand_flush)[0] == 0

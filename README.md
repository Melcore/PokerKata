Poker Hand est une implémentation en Python du Kata "Poker Hands" dont vous pouvez retrouvez la définition [sur codingdojo.org](https://codingdojo.org/kata/PokerHands/).

Pour l'utiliser, il suffit de lancer main.py
```shell
python main.py
```

Une invite de commande vous proposera alors de noter vos mains de la manière suivante

```shell
Mains : Black: 2H 3D 5S 9C KD  White: 2D 3H 5C 9S KH
```

Ce format a été choisit afin de pouvoir utiliser les exemples donner sur le site de codingdojo.

```python
Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C AH
Black: 2H 4S 4C 2D 4H  White: 2S 8S AS QS 3S
Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C KH
Black: 2H 3D 5S 9C KD  White: 2D 3H 5C 9S KH
```

Mais bien sûr, vous pouvez en utiliser d'autres.
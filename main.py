import re

from hand_utils import compare_hand


def construct_hand(list_of_cards):
    hand= {
        "color" : [],
        "value" : []
    }
    for card in list_of_cards:
        value, color = card[0], card[1]
        hand["color"].append(color)
        hand["value"].append(value)

    return hand


def split_input_to_hands(input):
    hand_1_cards = input.split(" ")[1:6]
    hand_2_cards = input.split(" ")[8:13]
    hand_1 = construct_hand(hand_1_cards)
    hand_2 = construct_hand(hand_2_cards)
    return hand_1, hand_2


if __name__ == "__main__":
    text_input = input("Mains : ")
    while not re.match('^[a-zA-Z]+:\s([2-9TJQKA][CDHS][\s]+){5}[\s]*[a-zA-Z]+:\s([2-9TJQKA][CDHS][\s]+){4}([2-9TJQKA][CDHS])[\s]*$', text_input):
        print("Erreur! Vérifiez que votre main est au bon format (Exemple 'Black: 2H 3D 5S 9C KD  White: 2D 3H 5C 9S KH'")
        text_input= input("Mains : ")

    result = compare_hand(*split_input_to_hands(text_input))
    print(result[1])


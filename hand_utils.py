from collections import Counter

colorOrder = ['C', 'D', 'H', 'S'] # useless but good for comprehension
valueOrder = ['2', '3', '4', '5','6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
handOrder = ['Hauteur', 'Paire', 'Double Paire', 'Brelan', 'Quinte', 'Couleur', 'Full', 'Carré', 'Quinte Flush']


def has_x_same_value(hand):
    """ Return value and count of maximum same value in hand """
    return Counter(hand["value"]).most_common(1)[0]


def is_following_value(hand):
    """ Return True if value are following in order """
    hand_sort = sorted(hand["value"], key= lambda value: valueOrder.index(value))
    for valueA, valueB in zip(hand_sort[:-1], hand_sort[1:]):
        if valueOrder.index(valueB) - valueOrder.index(valueA) != 1:
            return False
    return True


def is_same_color(hand):
    """ Return True if all cards of hand is same color """
    return len(set(hand["color"])) == 1


def has_x_y_same_value(hand):
    """ Return value and count of all multiple value in hand """
    count = Counter(hand["value"]).most_common()
    multiple = [ c for c in count if c[1] > 1]
    return multiple


def calcul_hand(hand):
    """ Return the value of the hand and all arguments needed in order in case of equality  """
    multiple_value = has_x_y_same_value(hand)
    flush = is_same_color(hand)
    straight = is_following_value(hand)

    if flush and straight:
        return "Quinte Flush", max(hand['value'], key=lambda value: valueOrder.index(value))

    if len(multiple_value) == 1 and multiple_value[0][1] == 4:
        quad_value = multiple_value[0][0]
        high = set(hand['value']) - {quad_value}
        high = list(high)[0]
        return "Carré", quad_value, high

    if len(multiple_value) == 2 and multiple_value[0][1] == 3:
        return "Full", multiple_value[0][0], multiple_value[1][0]

    if flush:
        return "Couleur", *sorted(hand["value"], key=lambda value: valueOrder.index(value), reverse=True)

    if straight:
        return "Quinte", max(hand['value'], key=lambda value: valueOrder.index(value))

    if len(multiple_value) == 1 and  multiple_value[0][1] == 3:
        set_value = multiple_value[0][0]
        high = set(hand['value']) - {set_value}
        high = sorted(list(high), key=lambda value: valueOrder.index(value), reverse =True)
        return "Brelan", multiple_value[0][0], *high

    if len(multiple_value) == 2:
        pairs_values = (multiple_value[0][0], multiple_value[1][0])
        pairs_sorted = sorted(pairs_values, key= lambda value: valueOrder.index(value), reverse=True)
        high = set(hand['value']) - set(pairs_values)
        high = list(high)[0]
        return "Double Paire", *pairs_sorted, high

    if len(multiple_value) == 1:
        pair_value = multiple_value[0][0]
        high = set(hand['value']) - {pair_value}
        high = sorted(list(high), key= lambda value: valueOrder.index(value), reverse=True)
        return "Paire", pair_value, *high

    return "Hauteur", *sorted(hand["value"], key= lambda value: valueOrder.index(value), reverse=True)


def compare_hand(hand_1, hand_2):
    """ Compare who win between two hands
        0 : equality,
        1 : hand 1 win
        2 : hand 2 win

        :string: : Win description
    """
    hand_1_value, *hand_1_args = calcul_hand(hand_1)
    hand_2_value, *hand_2_args = calcul_hand(hand_2)
    hand_1_value_ordering = handOrder.index(hand_1_value)
    hand_2_value_ordering = handOrder.index(hand_2_value)

    if hand_1_value_ordering > hand_2_value_ordering:
        return 1, f"La main 1 gagne avec {hand_1_value}"
    if hand_2_value_ordering > hand_1_value_ordering:
        return 2, f"La main 2 gagne avec {hand_2_value}"

    # it is possible to compare because 2 hands with same value have the same number of arguments.
    for i in range(len(hand_1_args)):
        arg_hand_1_ordering = valueOrder.index(hand_1_args[i])
        arg_hand_2_ordering = valueOrder.index(hand_2_args[i])
        if arg_hand_1_ordering > arg_hand_2_ordering:
            return 1, f"La main 1 gagne avec {hand_1_value} et une hauteur de {hand_1_args[i]}"
        if arg_hand_2_ordering > arg_hand_1_ordering:
            return 2, f"La main 2 gagne avec {hand_2_value} et une hauteur de {hand_2_args[i]}"

    return 0, "Les deux mains sont égales"

